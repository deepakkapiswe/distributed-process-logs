{-# LANGUAGE DeriveDataTypeable  #-}
{-# LANGUAGE DeriveGeneric       #-}
{-# LANGUAGE ScopedTypeVariables #-}
-- {-# LANGUAGE TemplateHaskell     #-}

module Main where
import           Control.Concurrent                                 (threadDelay)
import           Control.Distributed.Process                        hiding
                                                                     (Message)
import           Control.Distributed.Process.Backend.SimpleLocalnet
import           Control.Distributed.Process.Closure
import           Control.Distributed.Process.Node                   (initRemoteTable)
import           Control.Monad
import           Data.Binary
import           Data.Typeable
import           GHC.Generics                                       (Generic)
import           System.Environment                                 (getArgs)
import           System.Random
import           Text.Printf
import           Data.List                                          (delete)


data Message = Message ProcessId Int
 deriving (Eq,Show,Typeable,Generic)

instance Binary Message

data LogMessage = LogMessage {
                selfPid::ProcessId
              , senderPid :: ProcessId
              , val::Int
               }
 deriving (Eq,Typeable,Generic)

instance Binary LogMessage

instance Show LogMessage where
  show (LogMessage self sId val) = (f self) ++ (f sId) ++ (show val)
   where
     f::ProcessId->String
     f pid = "("++ (drop 23 $ show pid) ++") "


startReceiving::Process ()
startReceiving = receiveWait [
        match $ \(Message sId msg) -> do
                 self <- getSelfPid
                 say $ show (LogMessage self sId msg)
                 startReceiving
    ]

startSending::ProcessId->Int->[ProcessId]->[Int]->[Int]->[Int]->[Int]->Process ()
startSending self probability receivers (v:vals) (idx:idxs) (p:probs) (d:delays)= do
    when (p >= probability) $ send (receivers !! idx) (Message self v)
    liftIO. threadDelay $ d
    startSending self probability receivers vals idxs probs delays

master :: Int->Int->Int->Backend -> [NodeId] -> Process ()
master sendFor pCount prob  backend _ = do

  receiverPids <- forM [1..pCount] $ \_ ->
    spawnLocal startReceiving
  let randomIdxs = (randomRs (0::Int,pCount-2).mkStdGen) <$> [1..pCount]
      randomProbabilities = (randomRs (1::Int,100::Int).mkStdGen) <$> [1..pCount]
      randomDelays = (randomRs (1::Int,sendFor).mkStdGen) <$> [1..pCount]
      randoms_ = zip3 randomIdxs randomProbabilities randomDelays
  forM_ (zip receiverPids randoms_) $ \(pid,(rList,rProb,rDelay)) ->
    spawnLocal $ startSending pid (mod prob 100) (delete pid receiverPids) [1..] rList rProb rDelay

  say "All Nodes Started"
  say $ printf "sending messages for %d seconds" sendFor
  liftIO. threadDelay $ 1000000 * sendFor
  terminateAllSlaves backend

main :: IO ()
main = do
  args <- getArgs
  case args of
    [host, port,"--send-for",k,"--with-pCount",n,"--with-probability",p] -> do
      backend <- initializeBackend host port initRemoteTable
      startMaster backend (master (read k) (read n) (read p) backend)
    _-> print "Please provide valid arguments"
